﻿using Mbs.Utils.Sockets.Abstractions.Events;

namespace Mbs.Utils.Sockets.Abstractions
{
    public interface ITcpClientEventHandleMethods : INetEventMethods<ITcpConnection, NetClientEventArgs<ITcpConnection>>
    {
    }
}
