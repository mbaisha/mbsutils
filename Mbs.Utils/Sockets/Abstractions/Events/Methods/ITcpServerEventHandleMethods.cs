﻿using Mbs.Utils.Sockets.Abstractions.Events;

namespace Mbs.Utils.Sockets.Abstractions
{
    public interface ITcpServerEventHandleMethods : INetEventMethods<ITcpConnection, NetServerEventArgs>
    {
        void OnServerExceptionHandler(NetServerEventArgs args);
    }
}
