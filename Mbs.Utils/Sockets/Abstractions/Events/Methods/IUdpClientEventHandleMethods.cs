﻿using Mbs.Utils.Sockets.Abstractions.Events;

namespace Mbs.Utils.Sockets.Abstractions
{
    public interface IUdpClientEventHandleMethods : INetEventMethods<IUdpConnection, NetClientEventArgs<IUdpConnection>>
    {

    }
}