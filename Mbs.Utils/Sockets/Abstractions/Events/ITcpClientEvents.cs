﻿namespace Mbs.Utils.Sockets.Abstractions
{
    public interface ITcpClientEvents: IEvent<ITcpConnection, NetClientEventArgs<ITcpConnection>>
    {
    }
}