﻿namespace Mbs.Utils.Sockets.Abstractions
{
    public interface IUdpClientEvents : IEvent<IUdpConnection, NetClientEventArgs<IUdpConnection>>
    {
    }
}