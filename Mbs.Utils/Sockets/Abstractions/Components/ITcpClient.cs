﻿using Mbs.Utils.Sockets.Abstractions;
using System;

namespace Mbs.Utils.Sockets
{
    public interface ITcpClient : INetBase<ITcpConnection>, ITcpClientEvents, IDisposable
    {
        /// <summary>
        /// socket is connected
        /// </summary>
        bool Connected { get; }
        
        ITcpConnection Connection { get; }
    }
}