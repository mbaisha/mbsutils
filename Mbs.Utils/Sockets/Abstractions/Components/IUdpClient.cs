﻿using Mbs.Utils.Sockets.Abstractions;
using System;

namespace Mbs.Utils.Sockets
{
    public interface IUdpClient : INetBase<IUdpConnection>, IUdpClientEvents, IDisposable
    {
        IUdpConnection Connection { get; }
    }
}
