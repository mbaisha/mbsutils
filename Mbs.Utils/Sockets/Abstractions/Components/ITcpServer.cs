﻿using Mbs.Utils.Sockets.Abstractions;
using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace Mbs.Utils.Sockets
{
    public interface ITcpServer : INetBase<ITcpConnection>, ITcpServerEvents, IDisposable
    {
        Socket Instance { get; }

        IDictionary<string, ITcpConnection> Connections { get; }
    }
}