﻿namespace Mbs.Utils.Sockets.Abstractions
{
    public interface ITryConnectionStrategy
    {
        int Handle(int counter);
    }
}
