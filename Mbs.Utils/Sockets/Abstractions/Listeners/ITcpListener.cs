﻿using System;

namespace Mbs.Utils.Sockets.Abstractions
{
    public interface ITcpListener:INetListener<ITcpConnection>, IDisposable
    {
    }
}
