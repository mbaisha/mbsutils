﻿using System;

namespace Mbs.Utils.Sockets.Abstractions
{
    public interface IUdpListener : INetListener<IUdpConnection>, IDisposable
    {
    }
}
