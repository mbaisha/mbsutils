﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Mbs.Utils.Sockets
{
    public interface INetConnection : IDisposable
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        int Id { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; set; }

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        string Key { get; set; }
        /// <summary>
        /// Gets the Tag.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        dynamic Tag { get; set; }
        /// <summary>
        /// Gets the local endpoint of a Transmission Control Protocol (TCP) connection.
        /// </summary>
        IPEndPoint LocalEndPoint { get; }

        /// <summary>
        /// Gets the remote endpoint of a Transmission Control Protocol (TCP) connection.
        /// </summary>
        IPEndPoint RemoteEndPoint { get; }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        Socket Instance { get; }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops this instance.
        /// </summary>
        void Stop();


        //KeepAliveOptions KeepAliveOption { get; }
        //void KeepAlive(bool keepAlive = true, int interval = 5000, int span = 1000);
    }
}