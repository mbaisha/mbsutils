﻿using Mbs.Utils.Sockets.Abstractions;

namespace Mbs.Utils.Sockets.Tcp
{
    public class DefaultTryConnectionStrategy : ITryConnectionStrategy
    {
        public int Handle(int counter)
        {
            return (counter % 20) * 3000;
        }
    }
}