﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Mbs.Utils
{
    public  class HttpLite
    {
        private static readonly object LockObj = new object();
        private static HttpClient client = null;
        public HttpLite()
        {
            GetInstance();
        }
        public static HttpClient GetInstance()
        {

            if (client == null)
            {
                lock (LockObj)
                {
                    if (client == null)
                    {
                        client = new HttpClient();
                        client.Timeout = TimeSpan.FromSeconds(5);
                        //client.DefaultRequestHeaders.Add("x-www-foo", "123");
                    }
                }
            }
            return client;
        }
        public void setHeader(string key, string val)
        {
            client.DefaultRequestHeaders.Add(key, val);
        }
        public async Task<T> PostAsync<T>(string url, Dictionary<string, string> body) where T : class
        {
            var content = new MultipartFormDataContent();

            foreach (var k in body)
            {
                content.Add(new StringContent(k.Value), k.Key);
            }
            try
            {
                HttpResponseMessage res = await client.PostAsync(url, content);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    string json = res.Content.ReadAsStringAsync().Result;
                    return JsonHelper.DeserializeJsonToObject<T>(json);
                }
                else
                    return default(T);
            }
            catch (Exception ex)
            {
                string res = errorReport(ex);
                return default(T);
            }
        }

        public T Post<T>(string url, Dictionary<string, string> body) where T : class
        {
            try
            {
                var content = new MultipartFormDataContent();
                foreach (var k in body)
                {
                    content.Add(new StringContent(k.Value), k.Key);
                }
                Task<HttpResponseMessage> res = client.PostAsync(url, content);
                if (res.Result.StatusCode == HttpStatusCode.OK)
                {
                    string json = res.Result.Content.ReadAsStringAsync().Result;
                    return JsonHelper.DeserializeJsonToObject<T>(json);
                }
                else
                    return default;
            }
            catch (Exception ex)
            {
                string res = errorReport(ex);
                return default;
            }
        }

        public async Task<T> GetAsync<T>(string url) where T : class
        {
            try
            {
                //var responseString = await client.GetStringAsync(url);
                //return JsonHelper.DeserializeJsonToObject<T>(responseString);
                HttpResponseMessage res = await client.GetAsync(url);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    string json = res.Content.ReadAsStringAsync().Result;
                    return JsonHelper.DeserializeJsonToObject<T>(json);
                }
                else
                    return default(T);
            }
            catch (Exception ex)
            {
                string res = errorReport(ex);

                return default(T);
            }
        }

        public T Get<T>(string url) where T : class
        {
            try
            {
                Task<HttpResponseMessage> httpResponse = client.GetAsync(url);

                if (httpResponse.Result.StatusCode == HttpStatusCode.OK)
                {
                    string str = httpResponse.Result.Content.ReadAsStringAsync().Result;
                    return JsonHelper.DeserializeJsonToObject<T>(str);
                }
            }
            catch (Exception ex)
            {
                string res = errorReport(ex);
            }
            return default;
        }


        protected string errorReport(Exception e)
        {
            string str = "something wrong";
            //File.WriteAllText((DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000 + "error.txt", e.Message);
            return str;
        }

    }
}
