﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mbs.Utils.ThreadPool
{
    public class WorkTaskFactory
    {
        public ThreadPoolExecutor? ThreadPoolExcutor { get; private set; }
        public WorkTaskFactory(ThreadPoolExecutor excutor)
        {
            ThreadPoolExcutor = excutor;
        }

        public WorkTaskFactory()
        : this(new ThreadPoolExecutor(5, 10))
        {
        }

        public WorkTask StartNew(Action action, ThreadPoolExecutor? executor = null)
        {
            WorkTask task = new WorkTask(action);
            ThreadPoolExcutor = executor ?? ThreadPoolExcutor;
            ThreadPoolExcutor?.QueueTask(task);
            return task;
        }

        public WorkTask<TResult> StartNew<TResult>(Func<object?, TResult> func, object? state, ThreadPoolExecutor? executor = null)
        {
            WorkTask<TResult> task = new WorkTask<TResult>(func, state);
            ThreadPoolExcutor = executor ?? ThreadPoolExcutor;
            ThreadPoolExcutor?.QueueTask(task);
            return task;
        }
    }
}
