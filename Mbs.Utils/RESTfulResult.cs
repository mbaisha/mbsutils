﻿using Mbs.Utils.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mbs.Utils
{
    public class RESTfulResult
    {
        public static XnRestfulResult<object> Result(int statusCode, bool succeeded = default, object data = default, object message = default, object extras = default)
        {
            return new XnRestfulResult<object>
            {
                Success = succeeded,
                Code = statusCode,
                Message = message,
                Data = data,
                Extras = extras,
                Timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds()
            };
        }

        public static XnRestfulResult<object> SUCCESS(object data = default, object message = default, object extras = default)
        {
            return new XnRestfulResult<object>
            {
                Success = true,
                Code = 200,//Microsoft.AspNetCore.Http.StatusCode
                Message = message,
                Data = data,
                Extras = extras,
                Timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds()
            };
        }

        public static XnRestfulResult<object> Failed(int statusCode, object data = default, object message = default, object extras = default)
        {
            return new XnRestfulResult<object>
            {
                Success = false,
                Code = statusCode,//Microsoft.AspNetCore.Http.StatusCode
                Message = message,
                Data = data,
                Extras = extras,
                Timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds()
            };
        }

        public static XnRestfulResult<object> Failed(object data = default, object message = default, object extras = default)
        {
            return new XnRestfulResult<object>
            {
                Success = false,
                Code = 303,//Microsoft.AspNetCore.Http.StatusCode
                Message = message,
                Data = data,
                Extras = extras,
                Timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds()
            };
        }
    }
}
