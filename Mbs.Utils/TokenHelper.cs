﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mbs.Utils
{
    public class TokenHelper
    {

        static Random rnd = new Random();
        static int seed = 0;

        public static string Create(int x = 10)
        {
            var rndData = new byte[x];
            rnd.NextBytes(rndData);

            var seedValue = Interlocked.Add(ref seed, 1);
            var seedData = BitConverter.GetBytes(seedValue);

            var tokenData = rndData.Concat(seedData).OrderBy(_ => rnd.Next());
            return Convert.ToBase64String(tokenData.ToArray()).Replace("=", "").Replace("/", "").Replace("\\", "").Replace("+", "");
        }

        public static string Create(string k = "")
        {
            byte[] b = System.Text.Encoding.Default.GetBytes(k + Guid.NewGuid().ToString());
            for (int i = 0; i < b.Length; i++)
            {
                int x = i / 3;
                b[i] = (byte)(b[i] + (byte)x);
            }
            return Convert.ToBase64String(b).Replace("=", "").Replace("/", "").Replace("\\", "").Replace("+", "");
        }
    }
}