﻿//using System;
//using System.Collections.Generic;
//using System.Drawing;
//using System.Drawing.Imaging;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Mbs.Utils.ImageHelper
//{
//    public  class BitmapConvert
//    {
//        //图片转为base64编码的字符串
//        public static string ImgToBase64String(Bitmap bmp)
//        {
//            try
//            {
//                MemoryStream ms = new MemoryStream();
//                bmp.Save(ms,ImageFormat.Png);
//                byte[] arr = new byte[ms.Length];
//                ms.Position = 0;
//                ms.Read(arr, 0, (int)ms.Length);
//                ms.Close();
//                return Convert.ToBase64String(arr);
//            }
//            catch (Exception ex)
//            {
//                return null;
//            }
//        }
//        //threeebase64编码的字符串转为图片
//        public static Bitmap Base64StringToImage(string strbase64)
//        {
//            try
//            {
//                byte[] arr = Convert.FromBase64String(strbase64);
//                MemoryStream ms = new MemoryStream(arr);
//                Bitmap bmp = new Bitmap(ms);
//                ms.Close();
//                return bmp;
//            }
//            catch (Exception ex)
//            {
//                return null;
//            }
//        }
//    }
//}
