using Mbs.Utils.Extension;
using Mbs.Utils.Sockets;
using Mbs.Utils.Sockets.Extensions;
using Mbs.Utils.ThreadPool;
using NPOI.Util;

namespace SocketTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button3.Enabled = false;
            StartThread();
            button3.Enabled = true;

        }


        delegate void SetTextCallback(string text);
        private void SetText(string text)
        {
            if (this.richTextBox1.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                richTextBox1.AppendText(text + Environment.NewLine);
                richTextBox1.ScrollToCaret();

                //this.richTextBox1.Focus();
                //this.richTextBox1.Select(this.richTextBox1.TextLength, 0);
                //this.richTextBox1.ScrollToCaret();
            }
        }
        ThreadPoolExecutor poolExcutor = null;
        WorkTask<int>[] workTasks = null;
        List<Mbs.Utils.Sockets.TcpClient> tcpClients = new List<Mbs.Utils.Sockets.TcpClient>();
        bool Cancel = false;
        void StartThread()
        {
            if (workTasks != null)
            {
                Stop();
            }
            Cancel = false;
            poolExcutor = new(int.Parse(numCoreThread.Value.ToString()), int.Parse(numThreadNum.Value.ToString()), KeepAliveTimeout: 5000, QueueCount: 20);
            int count = int.Parse(numCount.Value.ToString());
            workTasks = new WorkTask<int>[count];
            for (int i = 0; i < count; i++)
            {
                if (Cancel == true) { 
                    break; 
                }
                var woker= WorkTask.Factory.StartNew(t =>
                {
                    int xx = int.Parse(t.ToString()!);
                    //Console.WriteLine($"Task Id:{Environment.CurrentManagedThreadId},Parameter:{t}");
                    //return t == null ? default(int?) : (int)t + 1;
                  
                    Mbs.Utils.Sockets.TcpClient client = new Mbs.Utils.Sockets.TcpClient(txtHost.Text, int.Parse(txtPort.Text), id: xx)
                    {
                        //ConnectTimeout = 2000,
                        //ReconnectEnable = true,
                        OnConnected = (c) =>
                        {
                            // Console.WriteLine($"{c.RemoteEndPoint} connected.");
                            SetText($"任务编号{c.NetConnection.Id} --> {c.RemoteEndPoint} connected.");
                          
                        },
                        OnReceived = (c) =>
                        {
                            SetText($"任务编号{c.NetConnection.Id} --> Received from {c.RemoteEndPoint}:" + string.Join(" ", c.Buffers.Select(x => x.ToString("X2")).ToArray()) + "-->" + System.Text.Encoding.Default.GetString(c.Buffers));
                            //Console.WriteLine($"Received from {c.RemoteEndPoint}:");
                            //Console.WriteLine(string.Join(" ", c.Buffers.Select(x => x.ToString("X2")).ToArray()));
                            //Console.WriteLine(System.Text.Encoding.Default.GetString(c.Buffers));
                            // c.NetConnection.Send(c.Buffers);
                        },
                        OnDisconnected = (c) =>
                        {
                            //Console.WriteLine($"{c.RemoteEndPoint} disconnected.");
                            SetText($"任务编号{c.NetConnection.Id} --> {c.RemoteEndPoint} disconnected.");
                        },
                        OnStarted = (c) =>
                        {
                            //Console.WriteLine($"{c.RemoteEndPoint} started.");
                            SetText($"任务编号{c.NetConnection.Id} --> {c.RemoteEndPoint} started.ready to send or receive data");
                            if (!txtMsg.Text.IsNullOrWhiteSpace())
                            {
                                SetText($"任务编号 {c.NetConnection.Id} --> 发送 {txtMsg.Text}.");
                                c.NetConnection.Send(txtMsg.Text);
                            }
                        },
                        OnStopped = (c) =>
                        {
                            //Console.WriteLine($"{c.RemoteEndPoint} stopped.");
                            SetText($"任务编号{c.NetConnection.Id} --> {c.RemoteEndPoint} stopped.");
                        },
                        OnException = (c) =>
                        {
                            //Console.WriteLine($"{c.RemoteEndPoint} exception:Message:{c.Exception.Message},StackTrace:{c.Exception.StackTrace.ToString()}.");
                            SetText($"任务编号{c.NetConnection.Id} --> {c.RemoteEndPoint} exception:Message:{c.Exception.Message},StackTrace:{c.Exception.StackTrace.ToString()}.");
                        },
                        OnTryConnecting = (c) =>
                        {
                            //Console.WriteLine($"try connect {c.Counter} ...");
                            SetText($"任务编号{c.NetConnection.Id} --> {c.LocalEndPoint} try connect 第 {c.Counter} 次....");
                        }
                    };
                    lock (tcpClients)
                    {
                        tcpClients.Add(client);
                    }
                    // set keep alive
                    client.UseKeepAlive(true, 500, 500);

                    // tr connnect stratagy
                    // client.UseTryConnectionStrategy(new DefaultTryConnectionStrategy());

                    try
                    {
                        client.Start();
                        Thread.Sleep(5);
                    }
                    catch (Exception ex)
                    {
                        SetText($"线程编号{xx} --> Message:{ex.Message},StackTrace:{ex.StackTrace.ToString()}");
                    }
                    //Console.ReadLine();
                    return 0;// t == null ? default(int?) : (int)t + 1;

                }, i, poolExcutor);
                workTasks[i] = woker;
                Application.DoEvents();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Stop();
        }

        void Stop()
        {
            button3.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            poolExcutor.CanceleAll();
            {
                Cancel = true;
            }
            Application.DoEvents();
            lock (tcpClients)
            {
                System.Threading.Thread.Sleep(500);
                foreach (var item in tcpClients)
                {
                    System.Threading.Thread.Sleep(20);
                    int id = item.Id;
                    item.Stop();
                    SetText($"Socket客户端编号：{id}-->已停止");
                    if (workTasks.Any(p => (int)p.State == id))
                    {
                        var task = workTasks.Single(p => (int)p.State! == id);
                        task.Cancle();
                        bool IsComplete = task.IsCompleted;
                        SetText($"任务编号{id}--> 已中止");
                    }
                    else
                    {
                        SetText($"任务编号{id}--> 早已停止");
                    }
                }
              
            }

            {
                tcpClients.Clear();
            }
            this.Cursor = Cursors.Arrow;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (var item in tcpClients)
            {
                if (!txtMsg.Text.IsNullOrWhiteSpace())
                {
                    SetText($"任务编号 {item.Id} --> 发送 {txtMsg.Text}.");
                    item.Connection.Send(txtMsg.Text);
                }
            }
        }
    }
}