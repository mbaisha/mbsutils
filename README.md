# Mbs.Util

#### 介绍
Mbs.Util库，包括常用的帮助类库，多线程，线程池，TCP Server ， Socket Server ，Socket Client

cd /
sudo wget https://download.visualstudio.microsoft.com/download/pr/8159607a-e686-4ead-ac99-b4c97290a5fd/ec6070b1b2cc0651ebe57cf1bd411315/dotnet-sdk-6.0.401-linux-x64.tar.gz
sudo mkdir -p /dotnet && sudo tar zxf dotnet-sdk-6.0.401-linux-x64.tar.gz -C /dotnet

echo "export DOTNET_ROOT=/dotnet" >> ~/.bashrc
echo "export PATH=$PATH:/dotnet" >> ~/.bashrc

source ~/.bashrc
