﻿// See https://aka.ms/new-console-template for more information


using Mbs.Utils.Sockets;
using Mbs.Utils.ThreadPool;
ThreadPoolExecutor poolExcutor = new(5, 100, KeepAliveTimeout: 5000, QueueCount: 20);
int count = 5;
WorkTask<int>[] workTasks = new WorkTask<int>[count];
for (int i = 0; i < count; i++)
{
    workTasks[i] = WorkTask.Factory.StartNew(t =>
    {
        //Thread.Sleep(200);
        //Console.WriteLine($"Task Id:{Environment.CurrentManagedThreadId},Parameter:{t}");
        //return t == null ? default(int?) : (int)t + 1;


        TcpClient client = new TcpClient("127.0.0.1", 40015)
        {
            //ConnectTimeout = 2000,
            //ReconnectEnable = true,
            OnConnected = (c) =>
            {
                Console.WriteLine($"{c.RemoteEndPoint} connected.");
            },
            OnReceived = (c) =>
            {
                //Console.WriteLine($"Received from {c.RemoteEndPoint}:");
                //Console.WriteLine(string.Join(" ", c.Buffers.Select(x => x.ToString("X2")).ToArray()));
                //Console.WriteLine(System.Text.Encoding.Default.GetString(c.Buffers));
                Console.WriteLine($"Received from {c.RemoteEndPoint}-->" + string.Join(" ", c.Buffers.Select(x => x.ToString("X2")).ToArray()) + "-->" + System.Text.Encoding.Default.GetString(c.Buffers));

                // c.NetConnection.Send(c.Buffers);
            },
            OnDisconnected = (c) =>
            {
                Console.WriteLine($"{c.RemoteEndPoint} disconnected.");
            },
            OnStarted = (c) =>
            {
                //Console.WriteLine($"{c.RemoteEndPoint} started.");
            },
            OnStopped = (c) =>
            {
                //Console.WriteLine($"{c.RemoteEndPoint} stopped.");
            },
            OnException = (c) =>
            {
                Console.WriteLine($"{c.RemoteEndPoint} exception:Message:{c.Exception.Message},StackTrace:{c.Exception.StackTrace.ToString()}.");
            },
            OnTryConnecting = (c) =>
            {
                Console.WriteLine($"try connect {c.Counter} ...");
            }
        };

        // set keep alive
        client.UseKeepAlive(true, 500, 500);

        // tr connnect stratagy
        // client.UseTryConnectionStrategy(new DefaultTryConnectionStrategy());

        try
        {
            client.Start();

            Console.WriteLine("Started");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Message:{ex.Message},StackTrace:{ex.StackTrace.ToString()}");
        }

        //Console.ReadLine();
        return 0;// t == null ? default(int?) : (int)t + 1;
    }, i, poolExcutor);
}

Console.ReadLine();






return;


    //ThreadPoolExecutor poolExcutor = new(5, 100, KeepAliveTimeout: 5000, QueueCount: 20);
    //int count = 20000;
    //WorkTask<int>[] workTasks = new WorkTask<int>[count];
    //for (int i = 0; i < count; i++)
    //{
    //    workTasks[i] = WorkTask.Factory.StartNew(t =>
    //    {
    //        //Thread.Sleep(200);
    //        //Console.WriteLine($"Task Id:{Environment.CurrentManagedThreadId},Parameter:{t}");
    //        //return t == null ? default(int?) : (int)t + 1;


    //        TcpClient client = new TcpClient("127.0.0.1", 40015)
    //        {
    //            //ConnectTimeout = 2000,
    //            //ReconnectEnable = true,
    //            OnConnected = (c) =>
    //            {
    //                Console.WriteLine($"{c.RemoteEndPoint} connected.");
    //            },
    //            OnReceived = (c) =>
    //            {
    //                Console.WriteLine($"Received from {c.RemoteEndPoint}:");
    //                Console.WriteLine(string.Join(" ", c.Buffers.Select(x => x.ToString("X2")).ToArray()));
    //                Console.WriteLine(System.Text.Encoding.Default.GetString(c.Buffers));
    //                // c.NetConnection.Send(c.Buffers);
    //            },
    //            OnDisconnected = (c) =>
    //            {
    //                Console.WriteLine($"{c.RemoteEndPoint} disconnected.");
    //            },
    //            OnStarted = (c) =>
    //            {
    //                Console.WriteLine($"{c.RemoteEndPoint} started.");
    //            },
    //            OnStopped = (c) =>
    //            {
    //                Console.WriteLine($"{c.RemoteEndPoint} stopped.");
    //            },
    //            OnException = (c) =>
    //            {
    //                Console.WriteLine($"{c.RemoteEndPoint} exception:Message:{c.Exception.Message},StackTrace:{c.Exception.StackTrace.ToString()}.");
    //            },
    //            OnTryConnecting = (c) =>
    //            {
    //                Console.WriteLine($"try connect {c.Counter} ...");
    //            }
    //        };

    //        // set keep alive
    //        client.UseKeepAlive(true, 500, 500);

    //        // tr connnect stratagy
    //        // client.UseTryConnectionStrategy(new DefaultTryConnectionStrategy());

    //        try
    //        {
    //            client.Start();

    //            Console.WriteLine("Started");
    //        }
    //        catch (Exception ex)
    //        {
    //            Console.WriteLine($"Message:{ex.Message},StackTrace:{ex.StackTrace.ToString()}");
    //        }

    //        //Console.ReadLine();
    //        return 0;// t == null ? default(int?) : (int)t + 1;
    //    }, i, poolExcutor);
    //}

    //Console.WriteLine("start main");
    //WorkTask.WaitAll(workTasks);
    //Console.WriteLine(workTasks.Sum(t => t.Result));
    //Console.ReadLine();


