﻿// See https://aka.ms/new-console-template for more information
using Mbs.Utils.Sockets;

Console.WriteLine("TcpClient Start!");
TcpClient client = new TcpClient("47.99.116.157", 6060) //new TcpClient("47.99.116.157",  6060)
{
    //ConnectTimeout = 2000,
    //ReconnectEnable = true,
    OnConnected = (c) =>
    {
        Console.WriteLine($"{c.RemoteEndPoint} connected.");
        c.NetConnection.Send("hello,welcome!",System.Text.Encoding.UTF8);
    },
    OnReceived = (c) =>
    {
        Console.WriteLine($"Received from {c.RemoteEndPoint}:");
        Console.WriteLine(string.Join(" ", c.Buffers.Select(x => x.ToString("X2")).ToArray()) + "-->" + System.Text.Encoding.UTF8.GetString(c.Buffers));
        // c.NetConnection.Send(c.Buffers);
    },
    OnDisconnected = (c) =>
    {
        Console.WriteLine($"{c.RemoteEndPoint} disconnected.");
    },
    OnStarted = (c) =>
    {
        Console.WriteLine($"{c.RemoteEndPoint} started.");
    },
    OnStopped = (c) =>
    {
        Console.WriteLine($"{c.RemoteEndPoint} stopped.");
    },
    OnException = (c) =>
    {
        Console.WriteLine($"{c.RemoteEndPoint} exception:Message:{c.Exception.Message},StackTrace:{c.Exception.StackTrace.ToString()}.");
    },
    OnTryConnecting = (c) =>
    {
        Console.WriteLine($"try connect {c.Counter} ...");
    }
};

// set keep alive
client.UseKeepAlive(true, 500, 500);

// tr connnect stratagy
// client.UseTryConnectionStrategy(new DefaultTryConnectionStrategy());

try
{
    client.Start();

    Console.WriteLine("Started");
}
catch (Exception ex)
{
    Console.WriteLine($"Message:{ex.Message},StackTrace:{ex.StackTrace.ToString()}");
}

Console.ReadLine();

client.Stop();

Console.WriteLine("Stopped");
Console.ReadLine();